package com.lamp.planner.domain

object Constants {
    const val EVENT_ID = "EVENT_ID"
    const val YESTERDAY = "Yesterday"
    const val TODAY = "Today"
    const val TOMORROW = "Tomorrow"

    const val FORMAT_TIME = "EE, dd MMM. yyyy"
    const val All_DAY = "FULL_DAY"

    const val ALL_DAY_Y = 1
    const val ALL_DAY_N = 0
}
