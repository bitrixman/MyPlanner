package com.lamp.planner.presentation.features.myday.adapter

import com.lamp.planner.presentation.DisplayableItem

data class TimeEventModel(val time: String) : DisplayableItem
